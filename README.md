# Hortonworks Spark Certification test pattern, question 1. #

My Hortonworks certification exam is closing (7.9.2018). I googled the repeated [question pattern](https://www.quora.com/Can-someone-share-the-pattern-of-Hortonworks-HDPCD-spark-certification-sample-questions) 
of this exam:

    1st task is to extract the data from csv files from hdfs, apply filters and ordering with specified columns.

    2nd task is similar as 1st but to retrieve the data from json.

    3rd and 4th tasks are on querying the hive tables, apply aggregate functionalities, conditions and ordering.

    5th is to use accumulator to perform word count.

    6th is to submit a spark job on yarn.

    7th is to extract 2 data-sets which contains common column. Apply filters and join the data-sets to generate the required output.

Looks very easy, anyway I decided to go through every question pattern, to be sure that I will smoke it.    
Let's start with the 1st task and test it on three samples...

## Task 1.  

**Task:** Three input files. Print out top three rows from all of them which contains the word "Spark" most often.    

**Solution:**     


    def countWord(line: String, word: String):Int= {
        val arr = line.split(" ");
        var count = 0;
    
        for (i <- 0 until arr.length) {
            if (arr(i).toLowerCase().trim().contains(word)) {
                count = count + 1;
            }
        }
    
        return count;
    }

    val file1RDD = sc.textFile("/tests/file1.txt");
    val file2RDD = sc.textFile("/tests/file2.txt");
    val file3RDD = sc.textFile("/tests/file3.txt");

    val file1PairsRDD = file1RDD.map(line=>(line, countWord(line, "spark")));
    val file2PairsRDD = file2RDD.map(line=>(line, countWord(line, "spark")));
    val file3PairsRDD = file3RDD.map(line=>(line, countWord(line, "spark")));

    val joinedRDD = file1PairsRDD.union(file2PairsRDD).union(file3PairsRDD);

    joinedRDD.collect();

    val sortedRDDJoined = sc.parallelize(joinedRDD.map(pair=>(pair._2,pair._1)).sortByKey(false).map(pair=>(pair._2, pair._1)).take(3));

    sortedRDDJoined.collect();
    
Zeppelin output:

    countWord: (line: String, word: String)Int
    file1RDD: org.apache.spark.rdd.RDD[String] = /tests/file1.txt MapPartitionsRDD[329] at textFile at <console>:29
    file2RDD: org.apache.spark.rdd.RDD[String] = /tests/file2.txt MapPartitionsRDD[331] at textFile at <console>:29
    file3RDD: org.apache.spark.rdd.RDD[String] = /tests/file3.txt MapPartitionsRDD[333] at textFile at <console>:29
    file1PairsRDD: org.apache.spark.rdd.RDD[(String, Int)] = MapPartitionsRDD[334] at map at <console>:33
    file2PairsRDD: org.apache.spark.rdd.RDD[(String, Int)] = MapPartitionsRDD[335] at map at <console>:33
    file3PairsRDD: org.apache.spark.rdd.RDD[(String, Int)] = MapPartitionsRDD[336] at map at <console>:33
    joinedRDD: org.apache.spark.rdd.RDD[(String, Int)] = UnionRDD[338] at union at <console>:43
    res54: Array[(String, Int)] = Array((We think of using Spark Spark is the best Spark is better then Mapreduce.,3), 
    (Spark and only Spark Nothing else.,2), (We use Apache Hive, Nifi and Oozie.,0), (Spark xxx Spark yyy Spark,3), 
    (xxx Spark Spark,2), (Spark Spark Spark Spark Spark,5), (yyyy Spark yyyy Spark zzzz Spark,3), 
    (1.Spark 2.Spark 3.Spark 4.Spark 6.Spark 9.Spark,6), (Spark Spark.,2), (Spark.,1))
    
    sortedRDDJoined: org.apache.spark.rdd.RDD[(String, Int)] = ParallelCollectionRDD[344] at parallelize at <console>:45
    res55: Array[(String, Int)] = Array((1.Spark 2.Spark 3.Spark 4.Spark 6.Spark 9.Spark,6), 
                                        (Spark Spark Spark Spark Spark,5), 
                                        (Spark xxx Spark yyy Spark,3))

**Idea:**    


- fileXPairsRDD contains RDD with pairs (line, count of "spark" word)
- sortedRDDJoined is classic pattern, switch RDD pairs, sortByKey, take(3)...Super easy.

## Task 2. Replace word in three RDD files 

**Task:** Three input files. Get new RDD containing input files with word "Spark" replaced by "##".    

**Solution:**    


    def replaceWords(line: String, word2replace: String, newString: String):String={
        return line.replace(word2replace, newString);
    }

    val file1RDD = sc.textFile("/tests/file1.txt");
    val file2RDD = sc.textFile("/tests/file2.txt");
    val file3RDD = sc.textFile("/tests/file3.txt");

    file1RDD.union(file2RDD).union(file3RDD).map(line=>replaceWords(line, "Spark", "##")).collect();    
    
Zeppelin output:
    
    replaceWords: (line: String, word2replace: String, newString: String)String
    file1RDD: org.apache.spark.rdd.RDD[String] = /tests/file1.txt MapPartitionsRDD[361] at textFile at <console>:29
    file2RDD: org.apache.spark.rdd.RDD[String] = /tests/file2.txt MapPartitionsRDD[363] at textFile at <console>:29
    file3RDD: org.apache.spark.rdd.RDD[String] = /tests/file3.txt MapPartitionsRDD[365] at textFile at <console>:29
    res57: Array[String] = Array(We think of using ## ## is the best ## is better then Mapreduce., 
    ## and only ## Nothing else., We use Apache Hive, Nifi and Oozie., ## xxx ## yyy ##, xxx ## ##, ## ## ## ## ##, yyyy ## yyyy ## zzzz ##, 
    1.## 2.## 3.## 4.## 6.## 9.##, ## ##., ##.)

## Task 3. Read HDFS file, do transformations, filtering, ordering 

**Task:**  Input CSV file of people. Get all females and order them by name in the descending order. Save the output in parquet format.    

**Solution:**    


    import org.apache.spark.sql.hive.HiveContext;

    var hc = new HiveContext(sc);

    hc.sql("CREATE EXTERNAL TABLE IF NOT EXISTS people(id Int, name String, gendre String, country String) "+
           " row format delimited "+
           " fields terminated by ','"+
           " location '/tests/people'"
    );

    val df = hc.sql("select * from people where gendre = 'female' ORDER BY name DESC");
    
    df.show();

    df.write.mode("overwrite").format("parquet").save("/tests/respeople");
    
Zeppelin output:

    import org.apache.spark.sql.hive.HiveContext
    hc: org.apache.spark.sql.hive.HiveContext = org.apache.spark.sql.hive.HiveContext@6fb0678c
    res71: org.apache.spark.sql.DataFrame = [result: string]
    df: org.apache.spark.sql.DataFrame = [id: int, name: string, gendre: string, country: string]
    +---+------+------+-------+
    | id|  name|gendre|country|
    +---+------+------+-------+
    |  3|Teresa|female|    GBR|
    |  4|   Rut|female|    USA|
    |  5|Roasie|female|    AUS|
    |  2| Cathy|female|    GBR|
    +---+------+------+-------+

HDFS content of /tests/respeople:

    [root@sandbox ~]# hdfs dfs -ls /tests/respeople
    Found 8 items
    -rw-r--r--   1 zeppelin hdfs          0 2018-08-27 19:56 /tests/respeople/_SUCCESS
    -rw-r--r--   1 zeppelin hdfs        446 2018-08-27 19:56 /tests/respeople/_common_metadata
    -rw-r--r--   1 zeppelin hdfs       2249 2018-08-27 19:56 /tests/respeople/_metadata
    -rw-r--r--   1 zeppelin hdfs        916 2018-08-27 19:56 /tests/respeople/part-r-00000-5854c069-52f3-4740-8121-eb3abda86c5b.gz.parquet
    -rw-r--r--   1 zeppelin hdfs        900 2018-08-27 19:56 /tests/respeople/part-r-00001-5854c069-52f3-4740-8121-eb3abda86c5b.gz.parquet
    -rw-r--r--   1 zeppelin hdfs        916 2018-08-27 19:56 /tests/respeople/part-r-00002-5854c069-52f3-4740-8121-eb3abda86c5b.gz.parquet
    -rw-r--r--   1 zeppelin hdfs        911 2018-08-27 19:56 /tests/respeople/part-r-00003-5854c069-52f3-4740-8121-eb3abda86c5b.gz.parquet
    -rw-r--r--   1 zeppelin hdfs        446 2018-08-27 19:56 /tests/respeople/part-r-00004-5854c069-52f3-4740-8121-eb3abda86c5b.gz.parquet
    [root@sandbox ~]# 

Idea:     

This is super short solution. I've put external hive table directly upon the input HDFS file. No case class needed.    
Df variable is an transformation dataframe which I saved in the parquet format.


Best regards

Tomas
